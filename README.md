# Ledger-cli Csv Toolbox

A set of tools to do various types of processing on csv data for use with Ledger-cli and its derivitaves.

This was born out of a need with my personal accounting workflow to manage a Ledger-cli style ledger repo with csv files as the primary data source (most journal files being generated and regenerated automatically)
