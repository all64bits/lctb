# ALL SCRIPTS

* Combine into single script providing both an interactive interface, and also control via getopt

# menu.sh

* Alter to either follow ledger-cli/hledger behaviour of looking in $LEDGER or $LEDGER_FILE or ~/.hledger.journal for main ledger journal file, or take an alternative journal file as an argument

# account-change.sh

* Use budget journal to concatenate to create full account list
* **Confirm before category change operation**

# account-rename.sh?

* Create script to control renaming of accounts?

# weekly-report.sh

* Include last week's expendature for each account/category
* Include a confirmation of the date the ledger data is current to

# allaccount-merge.sh

* What about when there is no master file? Do I need an "init" command?

# OSX set-up

* brew install bash & set as default
* brew install gnu sed & set as default
* how did I set up the "application"?
