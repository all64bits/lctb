#!/usr/bin/env bash

ledger_root="$1"
journalfile="$2"
truncate_account="$3"

trans_selection="unknown"			# restrict transactions to unknown entries at this stage

# Pre-cleanup:
rm /tmp/csv_full_list /tmp/csv_update &> /dev/null

# Reference journal-parse.sh even if toolchain is not in PATH
journal_parse="$(dirname "$0")/journal-parse.sh"

shopt -s globstar

# Get all accounts. If $truncate_account contains a value then restric to this
accounts="$(ledger -f "$journalfile" accounts ${truncate_account})"

# Keep only budget item accounts
budget_accounts="$(echo "$accounts" | sed -E -e '/assets:.*/d' -e '/opening.*balance/d' -e '/unknown/d')"

# Remove parent accounts for display list
display_list="$(echo "$budget_accounts" | sed -E -e 's/equity:expenses://g' -e 's/equity:revenue://g')"

while [ "$account_selection" == "" ]
do

	unset selection
	unset account_selection

	selection="$(ledger -f "$journalfile" print ${trans_selection} | "$journal_parse" | fzf)"

	[ "$selection" == "" ] && exit

	id="$(echo "$selection" | grep -Eo 'ID: ([0-9]|[a-z])*$' | awk '{print $2}')"

	description="$(echo "$selection" | sed -r 's/ID: ([0-9]|[a-z])*$//')"
	fzf_header="$(echo -e "\nChoose categeory for:\n$description\nHit [Esc] to go back")"

	# Get account sans parent account from user with fzf
	account_selection_short="$(echo "$display_list" | fzf --header="$fzf_header")"

	# Get full account name for user selection - but if the previous fzf command was cancelled by the user, repeat loop
	# When grepping with account_selection_short, ensure that this is at the line end to avoid deeper accounts in the tree
	! [ "$account_selection_short" == "" ] && account_selection="$(echo "$budget_accounts" | grep "$account_selection_short$")"
done

for i in "$ledger_root"/**/master.csv ; do

	# If current file is a master.csv file, merge it in to a full transaction list
	cat "$i" >> /tmp/csv_full_list
done

line_to_replace="$(grep "$id" /tmp/csv_full_list)"
line_replacement="$line_to_replace,\"$(echo "$account_selection")\""

for i in "$ledger_root"/**/master.csv ; do
	
	{
	while read line; do
		
		if echo "$line" | grep -Fq "$line_to_replace"; then
			echo "$line_replacement"
		else
			echo "$line"
		fi
	done < "$i"
	} > /tmp/csv_update

	cat /tmp/csv_update > "$i"
done

# Rebuild journal files
hledger-flow import "$ledger_root"

# Cleanup:
rm /tmp/csv_full_list /tmp/csv_update
