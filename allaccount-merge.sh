#!/bin/bash

# Reference luid-gen.sh even if toolchain is not in PATH 
luid_gen="$(dirname "$0")/luid-gen.sh"

# Reference csvmerge.sh even if toolchain is not in PATH 
csvmerge="$(dirname "$0")/csvmerge.sh"

shopt -s globstar

traversal_root="$1"

# Traverse through directory tree, passing the full path for each file to $i
for i in "$traversal_root"/**/* ; do

	# If current file is a master.csv file, run csvmerge.sh script on it and
	# merge.csv, which should be in the same directory
	if echo "$i" | grep -E '^.*/1-in/2[0-9][0-9][0-9]/master.csv$'; then

		# If no mergefile is available then don't do anything, otherwise
		# run csvmerge.sh on master.csv and the csv file in the adjacent merge dir
		[ -f "$(dirname "$i")/merge/"*".csv" ] && \
		"$csvmerge" "$(dirname "$i")/merge/"*".csv" "$i"

		# Add luid's to csv transactions which don't already have them
		"$luid_gen" "$i"
	fi

# TODO
# throw error if more than one csv in "$(dirname "$i")/../merge/
done

hledger-flow import "$traversal_root"
