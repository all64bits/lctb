#!/bin/bash

mergefile="$1"
masterfile="$2"

# Change all line endings to unix style. Needed for diffing, and dos line
# endings cause issues in other script processing
dos2unix -q "$mergefile"

while read line; do

	# If mergefile line already occurs in masterfile, add to dups list
	if grep -Fq "$line" "$masterfile" ; then

		echo "$line" >> /tmp/csvdups
	fi	
done < "$mergefile"

# Use grep to remove all dups from mergefile
cat "$mergefile" | grep -Fvx "$(cat /tmp/csvdups)" > /tmp/csv-dedup-mergefile

# Combine dedupped mergefile with masterfile & overwrite to masterfile
cat /tmp/csv-dedup-mergefile "$masterfile" > /tmp/csvmerge
cat /tmp/csvmerge > "$masterfile"

# cleanup
rm /tmp/csvdups /tmp/csv-dedup-mergefile /tmp/csvmerge "$mergefile"



#-----TODO 

#add error checking if mergefile start date is the same or later than masterfile end date:
#
#                If mergefile start date is same as masterfile end date, **do what?**
#
#                If mergefile start date is later than masterfile end date, check balance assertions between the two interfacing transactions. If there is a difference error out of script.
