#!/usr/bin/env bash

# ${1:-....} takes $1 argument if this is defined, otherwise the standard
#	input of the own process is used
infile="${1:-/dev/stdin}"

# Convert newlines to "^", then convert "^^" back to newlines - records
#	in the journal format are multi-line but separated by double newline
list="$(tr '\n' ^ < "$infile" | sed 's/\^\^/\n/g')"

while read line; do

	# Grep for date string + general string up to first "^"
	# The last sed command strips the luid if the jounal entry has this
	#	on the same line
	date_desc="$(echo "$line" | grep -Eo '^[0-9]{4}/[0-9]{2}/[0-9]{2} ([^\^]*\^)' | rev | cut -c 2- | rev | sed -E 's/; luid\{[0-9a-zA-Z-]*\}//g')"

	# If date & description is longer than 78 characters, truncate it
	if [ "${#date_desc}" -gt 78 ]; then
		# When truncating, remove last three characters and replace with "..."
		trunc_desc="$(echo "$date_desc" | head -c 75)..."
	else
		# Otherwise pad the end of the string with whitespace to bring it to 78 char width
		let desc_ln=78-"${#date_desc}"
		desc_ln=$((desc_ln + ${#date_desc}))
		trunc_desc="$(printf "%-${desc_ln}s" "$date_desc")"
	fi

	# Get all currency amounts in record. This produces a multi-line output when
	#	there are multiple hits. Filter for first hit.
	account_amount="$(echo "$line" | grep -Eo '\$-?[0-9]+\.[0-9][0-9]' | head -n 1)"

	# If the current line happens to be the last in the journal file, the "account amount" may include a
	#	trailing "^" from the line splitting stage. Remove this if it exists
	echo "$account_amount" | grep -q '\^' && account_amount="$(echo "$account_amount" | grep -Eo '.*[^\^]')"

	if [ "${#account_amount}" -gt 7 ];then
		justify_amt="$account_amount"
	else
		justify_amt="$(echo -e "$account_amount\t")"
	fi

	# Grep for luid{xxxx} & then trim parentheses
	luid="$(echo "$line" | grep -Eo 'luid{[0-9a-zA-Z-]*}' | cut -c 6- | rev | cut -c 2- | rev)"

	# Output processed line
	echo -e "$trunc_desc\t$justify_amt\t ID: $luid"
done < <(echo "$list")
