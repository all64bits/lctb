#!/bin/bash

infile="$1"

{
while read line; do
	# If mergefile line already occurs in masterfile, add to dups list
	if echo "$line" | grep -Eq '^.*luid{[0-9a-zA-Z-]*}' ; then
		
		echo "$line"
	else

		luid=",\"luid{$(echo "$line" | md5sum | tr -d '  -')}\""
		echo "$line$luid"
	fi	
done < "$infile"
} > /tmp/luidcsv

cat /tmp/luidcsv > "$infile"

# Garbage:
		#luid="$(echo "$line" | tr -d '\n')"
		#raw_csv="$(echo $line | grep -Eo '^(((\"([^\"]|\"\")*\")|[^,])*(,|$)){4}')"
