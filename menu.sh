#!/usr/bin/env bash
# A menu driven shell script sample template 
## ----------------------------------
# Step #1: Define variables
# ----------------------------------

ledger_root="$1"

# Save the user's terminal screen.
printf '\e[?1049h'
 
# Reference account-change.sh even if toolchain is not in PATH
account_change="$(dirname "$0")/account-change.sh"

# Reference trans-split.sh even if toolchain is not in PATH
trans_split="$(dirname "$0")/trans-split.sh"

# ----------------------------------
# Step #2: User defined function
# ----------------------------------
pause(){
  read -p "Press [Enter] key to continue..." fackEnterKey
}

one(){

	clear
	# account_change.sh requires root dir of ledger repo as 1st arg, and the path
	# of the main journal file in that repo as the second arg.
	"$account_change" "$ledger_root" "$WFL/incl-start-bals.gitkeep.journal"
}
 
two(){

	clear
	# account_change.sh requires root dir of ledger repo as 1st arg, and the path
	# of the main journal file in that repo as the second arg.
	"$trans_split" "$ledger_root" "$WFL/incl-start-bals.gitkeep.journal"
}

# do something in two()
three(){
	echo "Under construction"
        pause
}

out(){
	# Restore the user's terminal screen.
	printf '\e[?1049l'
	# escapes for loading/restoring terminal screen don't seem to be working?
	clear
	exit 0
}

 
# function to display menus
main_menu() {
	clear
	echo ""	
	echo " Ledger-cli: Csv Toolbox"
	echo "------------------------------"
	echo ""
	echo "1. Manually assign account to transaction"
	echo "2. Split transaction & assign to accounts"
	echo "3. Placeholder menu item"
	echo "4. Exit"
	echo ""
}
# read input from the keyboard and take a action
# invoke the one() when the user select 1 from the menu option.
# invoke the two() when the user select 2 from the menu option.
# Exit when user the user select 3 form the menu option.
main_options(){
	local choice
	read -p "Enter choice [ 1 - 4 ] " choice
	case $choice in
		1) one ;;
		2) two ;;
		3) three;;
		4) out;;
#		*) echo -e "Error..." && sleep 2
	esac
}

# ----------------------------------------------
# Step #3: Trap CTRL+C, CTRL+Z and quit singles
# ----------------------------------------------
#trap '' SIGINT SIGQUIT SIGTSTP
 
# -----------------------------------
# Step #4: Main logic - infinite loop
# ------------------------------------
while true
do
 
	main_menu
	main_options
done
