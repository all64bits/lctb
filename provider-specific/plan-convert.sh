#!/usr/bin/env bash

# The sticking point for this script executing correctly is how the debit & credit 
# columns are identified. The output from pdftotext has no delineation between
# columns, so the fields need to be extracted by first calling out the fields at
# the beginning and end of each line. The description is ok because it is a string
# of n characters after the date which is a fixed length from the start of the line.
# The balance is similarly a string of n chars back from the end of the line, which
# leaves the credit & debit columns in the middle. The frustrating thing is that
# the spacing around these seems to vary, in some cases significantly.
#
# if you need to troubleshoot this, uncomment the "amountstring: $amount_string"
# line and inspect the resulting csv files. Then adjust the string widths in the
# sed commands for the $amount_sign evaluation accordingly.

scan_rootdir="$1"

! [ -d /tmp/plan-convert ] && mkdir /tmp/plan-convert

pdf_convert(){

	infile="$1"

	# Get file name of current pdf (without extension). The last sed commands are to strip out
	# parentheses if filename is of the form; "Loan Statement(2).pdf" - is this required?
	statement_filename="$(basename "$1" | sed -E -e 's/^(.*)\.pdf$/\1/' -e 's/\(//' -e 's/\)//' )"

	# is this required?
	cp "$infile" /tmp/plan-convert/"$statement_filename".pdf

	no_of_pages="$(pdftotext "$infile" - | grep -E -m 1 "Page [0-9]* of [0-9]*" | awk '{print $4}')"

	# Trim leading & trailing boilerplate info on each page and write to merged.txt, then
	# pass this to loop to parse the transaction fields
	# We don't want to run pdfseparate on infile if it is a single page; it results in two pdfs;
	# filename.pdf & filename-1.pdf
	if [ "$no_of_pages" == "1" ]; then

		pdftotext -layout "$infile" - | tail -n +22 | head -n -36 > /tmp/plan-convert/merged.txt
	else

		# develop this section to run a loop for situations where there is more than 2 pages?
		pdfseparate /tmp/plan-convert/"$statement_filename".pdf /tmp/plan-convert/"$statement_filename"-%d.pdf

		pdftotext -layout /tmp/plan-convert/"$statement_filename"-1.pdf
		pdftotext -layout /tmp/plan-convert/"$statement_filename"-2.pdf

		cat /tmp/plan-convert/"$statement_filename"-1.txt | tail -n +22 | head -n -9 > /tmp/plan-convert/merged.txt
		cat /tmp/plan-convert/"$statement_filename"-2.txt | tail -n +2 | head -n -35 >> /tmp/plan-convert/merged.txt
	fi

	cat /tmp/plan-convert/merged.txt | sed 's/^ *//g' \
	| while read line_raw; do

		columntrim=0

		# If there is a significant amount of space between the date and the description this seems
		# to indicate that all of the fields are spaced out further to the right
		[[ "$line_raw" =~ ^[0-9]{2}-[A-Z][a-z]{2}-[0-9]{4}[[:space:]]{7}.*$ ]] && columntrim=1

		line="$(echo "$line_raw" | sed 's/\(^.\{11\}\) */\1"/')"

		# Extract the last section of line containing balance, remove whitespace
		# and surround in quotes
		balance="\"-$(echo "$line" | grep -Eo ".{14}$" | sed 's/^ *//g' | sed 's/,//g')\""

		# Extract the first section of line containing date & description
		date_desc="$(echo "$line" | grep -Eo "^.{74}" | sed 's/ *$//g')\""

		# Trim leading date string to get description only
		description="$(echo "$date_desc" | sed 's/^.\{11\} *//g')"

		# Extract only date string and convert to expected format
		date="$(date -d "$(echo "$date_desc" | grep -Eo "^.{11}")" +%d/%m/%Y)"

		# Extract middle section of line containing amount in either +ve or -ve columns
		amount_string="$(echo "$line" | sed 's/^.\{74\}//g' | sed 's/.\{14\}$//g')"
#echo "amountstring: $amount_string"
# above line is for testing purposes

		# Search for number within left hand colum of amount_string - this means
		# that amount is -ve; otherwise set to +ve
		# The .{n} numbers below need to be calibrated to locate one of the two decimal
		# digits in the amounts in the correct column
		if [ "$columntrim" == 0 ]; then
			amount_sign="$(echo "$amount_string" | sed 's/^.\{34\}\(.\{7\}\)/\1/' | grep -Eq "[0-9\.,]" && echo "+" || echo "-")"
		else
			# .{65} is a guess for the correct position at the moment
			amount_sign="$(echo "$amount_string" | sed 's/^.\{55\}\(.\{7\}\)/\1/' | grep -Eq "[0-9\.,]" && echo "+" || echo "-")"
		fi

		# Extract only amount, removing whitespace. Then prepend amount_sign &
		# surround in quotes
		amount="\"$amount_sign$(echo "$amount_string" | grep -Eo "([0-9]|,)+(\.[0-9]*)?" | sed 's/,//g')\""

		# Output line in the required column order
		printf "%s\n" "$date,$amount,$description,$balance"

	# Use sed to reverse transaction order & then pipe output to csv file
	done | sed '1!G;h;$!d' > "$(dirname "$infile")/plan-parsed.csv"
}

shopt -s globstar

for i in "$scan_rootdir"/**/1-in/**/merge/*.pdf ; do

	echo "$i"
	pdf_convert	"$i"

	# Remove original pdf once converted
	rm "$i"
done

# Cleanup
rm -r /tmp/plan-convert
