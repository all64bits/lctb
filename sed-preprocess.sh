#!/usr/bin/env bash

# Pass this script your merge file prior to LCTB processing

# Remove dollar signs from values
sed -i 's/\$//g' "$1"

# Remove thousands separators from values - ONLY WORKS FOR VALUES UP TO 999,999,999.99!!
sed -i -E "s/\"(\+?-?[1-9]+),([0-9]{3})(,([0-9]{3}))*(\.[0-9]{2})?\"/\1\2\4\\5/g" "$1"
