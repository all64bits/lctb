#!/usr/bin/env bash

traversal_root="$1" # as per allaccount-merge.sh
journalfile="$2" # as per account-change.sh

# Define date format regex for transaction csv's
date_format="^[0-9]{2}/[0-9]{2}/[0-9]{4}"

report_query="unknown"			# restrict transactions to unknown entries at this stage

ts_acctname="assets:transaction_splits"

subtrans_bal=""

# Reference journal-parse.sh even if toolchain is not in PATH
journal_parse="$(dirname "$0")/journal-parse.sh"

accounts_list(){

# Get all accounts. If $truncate_account contains a value then restric to this
accounts="$(ledger -f "$journalfile" accounts ${truncate_account})"

# Keep only budget item accounts
budget_accounts="$(echo "$accounts" | sed -E -e '/assets:.*/d' -e '/opening.*balance/d' -e '/unknown/d')"

# Remove parent accounts for display list
display_list="$(echo "$budget_accounts" | sed -E -e 's/equity:expenses://g' -e 's/equity:revenue://g')"
}

trans_selector () {

	unset total_sign

	selection="$(ledger -f "$journalfile" p ${report_query} | "$journal_parse" | fzf)"

	# If fzf provides no output then the user has hit escape; exit script
	[ "$selection" == "" ] && clear && exit

	description="$(echo "$selection" | sed 's/\$-\?[0-9]*\.[0-9]\{2\}[[:space:]]*ID:.*$//')"

	total_actual="$(echo "$selection" | sed 's/.*\$\(-\?[0-9]*\.[0-9]\{2\}\)[[:space:]]*ID:.*$/\1/')"
	echo "$total_actual" | grep -Eq "\-.*" && total="$(echo "$total_actual" | sed 's/^-\(.*\)$/\1/')" && total_sign="-"
	! echo "$total_actual" | grep -Eq "\-.*" && total="$total_actual" && total_sign="+"

	echo "     None" > /tmp/TR_subtrans_nl
}

pause(){ read -p "Press [Enter] key to continue..." fackEnterKey; }

one(){ subtrans_add; }

two(){ subtrans_rm; }

three(){

	if [ "$total_actual" == 0 ]; then

		if [ "$total_sign" == "+" ]; then
			total_sign_inv="-"
		else
			total_sign_inv="+"
		fi

		subtrans_summation="$(grep -E "$selection_id" "$csv_sourcefile" | \
		sed "s/^\([^,]*,\"\)[-+][0-9]*\.[0-9]\{2\}\(\"\?,[^,]*,\)[^,]*\(,\"\?luid{\)[0-9a-z]*\(}.*\)/\1$total_sign_inv$total\2\"\"\3TS$ts_base-X\4,\"$ts_acctname\"/g")"

		sed -i "s/^\(.*luid{\)$selection_id\(}.*\)/\1TS$ts_base-0\2,\"$ts_acctname\"/g" "$csv_sourcefile"

		# Combine subtransactions, subtrans_summation (in that order)
		echo -e "$(cat /tmp/TR_subtrans_nl)\n$subtrans_summation" > /tmp/TR_subtrans

		# Combine subtransaction list, existing split file (in that order)
		# and save to split file
		cat /tmp/TR_subtrans "$csv_splitfile" > /tmp/TR_subtrans_nl
		cp /tmp/TR_subtrans_nl "$csv_splitfile"

		# Cleanup & exit
		hledger-flow import "$WFL"
		rm /tmp/TR_subtrans
		rm /tmp/TR_subtrans_nl
		exit
	else

		echo "Error: sum of subtransactions does not equal parent transaction total!"
		pause
	fi
}

four(){

	# Clear subtransaction list to re-start
	rm /tmp/TR_subtrans
	rm /tmp/TR_subtrans_nl

	# ...and return to main transactions list
	trans_selector
}

print_main_header(){

	clear

	echo ""	
	echo "Selected Parent Transaction:"
	echo "$description"
	echo ""
	echo "Remaining transaction total = \$$total_sign$total"
	echo "$total_actual"
	echo ""
	echo "---"
	echo ""	
	echo "Sub-transactions:"
	cat /tmp/TR_subtrans_nl | sed 's/^[^,]*,"\([^,]*\)","\([^,]*\)",.*TS[0-9]\{5\}-\([0-9]*\)}","\([^,]*\)"$/    \3: \1 \2\n    > \4/g'
	echo ""
	echo "---"
	echo ""
}

print_subtrans_header(){

		print_main_header

		echo "Add New Subtransaction"
		echo ""
}

main_menu() {

	print_main_header

	echo "1. Add new sub-transaction"
	echo "2. Delete sub-transaction"
	echo "3. Commit listed subtransactions to ledger"
	echo "4. Cancel all & return to selection list"
	echo ""
}

main_options(){
	local choice
	read -p "Enter choice [ 1 - 4 ] " choice
	case $choice in
		1) one ;;
		2) two ;;
		3) three ;;
		4) four ;;
#		*) echo -e "Error..." && sleep 2
	esac
}

split_ID_base () {

	shopt -s globstar
	for i in "$traversal_root"/**/*.csv ; do
		grep --color=never -Eo "TS[0-9]{5}" "$i" >> /tmp/cat-csv
	done

	sort /tmp/cat-csv > /tmp/splitID-sorted

	ts_base="$(printf "%05d" "$(echo "$(cat /tmp/splitID-sorted | tail -1 | grep --color=never -Eo "[0-9]$")" + 1 | bc)")"
	rm /tmp/splitID-sorted
	rm /tmp/cat-csv
}

csv_source(){

	# Determine source csv file for selection

	# Get ID for parent transaction
	selection_id="$(echo "$selection" | sed 's/^.*ID: \([0-9a-z]*\)$/\1/g')"

	# Get path of source file for selected transaction
	csv_sourcefile="$(grep --color=never -l -R --exclude-dir=.git "$selection_id" "$traversal_root" | grep -Eo "^.*\.csv$")"

	date="$(grep -E "$selection_id" "$csv_sourcefile" | grep -Eo "$date_format")"

	# Determine save file for subtransactions
	csv_splitfile="$(dirname "$csv_sourcefile")/splits.csv"
}

subtrans_add_amt(){
	while ! [[ "$subtrans_amt" =~ ^(c|C)$ ]]
	do

		print_subtrans_header

		read -p "Please enter subtransaction amount [ or \"c\" to cancel ]: " subtrans_amt

		# If user inputs "cancel" then exit to previous calling shell
		[[ "$subtrans_amt" =~ ^(c|C)$ ]] && subtrans_amt="" && break

		# If input is of zero value, or non-numerical, or has precision greater than 2 then error
		if ! [[ "$subtrans_amt" =~ ^(([0-9]+)((\.[0-9]{1,2})?))$ ]] || \
		[[ "$subtrans_amt" == "0.00" ]] || \
		[[ "$subtrans_amt" == "0.0" ]] || \
		[[ "$subtrans_amt" == "0" ]]; then

			print_subtrans_header

			echo "Error: Please enter a non-zero amount (max 2 decimal places)"

			# Clear function's key variable and re-run loop
			pause && subtrans_amt="" && continue
		fi

		subtrans_add_sign
		break
	done
}

subtrans_add_sign(){
	while ! [[ "$subtrans_sign" =~ ^(c|C)$ ]]
	do

		print_subtrans_header

		read -p "Amount = \$$subtrans_amt; is this positive or negative? [ enter + or -, or \"c\" to cancel ]: " subtrans_sign

		# If user inputs "cancel" then exit to previous calling shell
		[[ "$subtrans_sign" =~ ^(c|C)$ ]] && subtrans_sign="" && break

		# If input is anything apart from "-" or "+" then error
		if ! [[ "$subtrans_sign" =~ ^(\+|\-)$ ]]; then

			print_subtrans_header

			echo "Error: Please enter either \"+\" or \"-\" only"

			# Clear function's key variable and re-run loop
			pause && subtrans_sign="" && continue
		fi

		subtrans_act_amt="$subtrans_amt"
		[ "$subtrans_sign" == "-" ] && subtrans_act_amt="$subtrans_sign$subtrans_amt"

		if (( $(echo "$subtrans_act_amt < $total_actual" | bc) )); then
			echo "Warning: the current subtransaction amount exceeds the remaining parent transaction amount!"
			pause
		fi

		subtrans_add_desc
		break
	done
}

subtrans_add_desc(){
	while ! [[ "$subtrans_sign" =~ ^(c|C)$ ]]
	do

		print_subtrans_header

		read -p "Amount = $subtrans_sign\$$subtrans_amt; please enter a description for this subtransaction [ or \"c\" to cancel] : " subtrans_desc

		# If user inputs "cancel" then exit to previous calling shell
		[[ "$subtrans_desc" =~ ^(c|C)$ ]] && subtrans_sign="" && break

		if [[ "$subtrans_desc" == "" ]]; then

			print_subtrans_header

			echo "Error: Please enter a description"

			# Clear function's key variable and re-run loop
			pause && subtrans_desc="" && continue
		fi

		subtrans_acct
		break
	done
}

subtrans_acct(){
	fzf_header="$(echo -e "\nChoose categeory for:\n"$subtrans_sign\$$subtrans_amt $subtrans_desc"\nHit [Esc] to cancel")"

	# Get account sans parent account from user with fzf
	account_selection_short="$(echo "$display_list" | fzf --header="$fzf_header")"

	# Get full account name for user selection - but if the previous fzf command was cancelled by the user, repeat loop
	# When grepping with account_selection_short, ensure that this is at the line end to avoid deeper accounts in the tree
	! [ "$account_selection_short" == "" ] && account_selection="$(echo "$budget_accounts" | grep "$account_selection_short$")"

		# End loop
		break
}

subtrans_add(){

	# Start the user prompt series for subtransaction details; starting with amount
	subtrans_add_amt

	total_actual="$(echo "$total_actual - $subtrans_act_amt" | bc)"

	if ! [ "$subtrans_amt" == "" ] && \
	! [ "$subtrans_sign" == "" ] && \
	! [ "$subtrans_desc" == "" ]; then
		# Whatever precision was given for subtrans_amt, make it a precision of 2
		subtrans_amt="$( echo "$subtrans_amt+0.00" | bc )"

		# If there is not yet any subtransactions in the tmp file, clear it
		[ "$(cat /tmp/TR_subtrans_nl)" == "     None" ] && cat /dev/null > /tmp/TR_subtrans_nl

		echo -e "$date,\"$subtrans_sign$subtrans_amt\",\"$subtrans_desc\",\"$subtrans_bal\",\"luid{TS$ts_base-}\",\"$account_selection\"" >> /tmp/TR_subtrans

		nl -s ":" /tmp/TR_subtrans | sort -r | \
		sed 's/^ *\([0-9]*\):\(.*luid{TS[0-9]\{5\}-\)\(.*\)$/\2\1\3/g' \
		> /tmp/TR_subtrans_nl
	fi
}

subtrans_rm(){

	while ! [[ "$subtrans_lineno" =~ ^(c|C)$ ]]
	do

	print_main_header

	read -p "Please enter the line number of the subtransaction you wish to remove; \"a\" for all [ or \"c\" to cancel]: " subtrans_lineno

	# If user inputs "cancel" then exit to previous calling shell
	[[ "$subtrans_lineno" =~ ^(c|C)$ ]] && subtrans_lineno="" && break

	if [[ "$subtrans_lineno" =~ ^(a|A)$ ]]; then
		
		echo "     None" > /tmp/TR_subtrans_nl
		rm /tmp/TR_subtrans
	else	

		subtrans_lineno="$(echo "$subtrans_lineno+0" | bc)"

		# Remove selected line from subtransactions list
		sed -i "/^.*luid{TS[0-9]\{5\}-$subtrans_lineno.*/d" /tmp/TR_subtrans_nl

		if [ "$(cat /tmp/TR_subtrans_nl)" == "" ] || [ "$(cat /tmp/TR_subtrans_nl)" == "     None" ]; then

			# If subtransaction removal results in an empty file, change the description
			echo "     None" > /tmp/TR_subtrans_nl
			rm /tmp/TR_subtrans
		else 

			# Remove line numbers from subtransactions list
			cat /tmp/TR_subtrans_nl | sed 's/^\(.*luid{TS[0-9]\{5\}-\)[0-9]*\(}.*\)$/\1\2/g' > /tmp/TR_subtrans

			# Re-add corrected line numbers to subtransactions list
			nl -s ":" /tmp/TR_subtrans | \
			sed 's/^ *\([0-9]*\):\(.*luid{TS[0-9]\{5\}-\)\(.*\)$/\2\1\3/g' \
			> /tmp/TR_subtrans_nl
		fi
	fi

		# End loop
		break
	done
}

accounts_list

split_ID_base

trans_selector

csv_source

while true
do
	main_menu
	main_options
done
