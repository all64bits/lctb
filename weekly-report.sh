#!/usr/bin/env bash

ledger_file="$1"
budget_file="$2"
account_list="$3"

email_header_file="$4"

cat_bal(){
	category="$1"

	remaining_weeks="$(echo "52 - $(date +%U)" | bc)"

	# The awk portion of this is some black magic to grab the last line of the input and print the $1 word
	annual_budget="$(ledger b -f "$budget_file" --budget -b "$(date +%Y)-01-01" -e "$(date +%Y)-12-31" "$category" | awk '/./{line=$0} END{print $1}')"

	# This is a bashism which means: starting at character 1 of annual_budget (char 0 being the left-most one)
	# return the remainder of the string
	annual_budget="${annual_budget:1}"

	# Regarding the sed command: The 5th row down is the first line of the actual budget table content
	# It contains the subtotal for the full set described by $category; whether $category is a single
	# account, or multiple
	budget_report_raw="$(hledger balance -f "$ledger_file" -f "$budget_file" -b "$(date +%Y)-01-01" --budget "$category" | sed '5q;d')"

	actual="$(echo $budget_report_raw | awk '{printf $3}' | grep -Eo "\-?[0-9]+(\.[0-9]{2})?")"
	budget="$(echo $budget_report_raw | grep -Eo '\-?[0-9]+\.?[0-9]{2}\]' | grep -Eo "\-?[0-9]+(\.[0-9]{2})?")"

	annual_remaining="$(echo "$annual_budget - $actual" | bc)"
	remaining_wkly_spend="$(echo "$annual_remaining / $remaining_weeks" | bc)"

	curr_balance="$(echo -"$actual - $budget" | bc)"
	pcent_difference="$(echo "scale=2; $annual_remaining / $budget * 100" | bc | awk -F "." '{printf $1}')"

	echo ""
	echo ""
	echo "--==: $category :==--"
	echo ""
	echo "Current Balance: $curr_balance"
	echo ""
	echo "$annual_remaining" | grep -qv \- && echo "\$$remaining_wkly_spend may be spent per week for the remainder of the year to stay within budget."
	echo "$annual_remaining" | grep -q \- && echo -e "Warning: Total annual budget already exceeded by $pcent_difference%.\nPlease avoid additional expendature in this category."
}

! [ "$email_header_file" == "" ] && cat "$email_header_file"
! [ "$email_header_file" == "" ] && echo "Subject: Budget Update $(date +%A,\ %B\ %d\ %Y)"
echo "$(hledger -f "$ledger_file" r unknown | wc -l) uncategorised transactions are outstanding."
echo "----------------------"
while read line; do

	cat_bal "$line"
done < "$account_list"
